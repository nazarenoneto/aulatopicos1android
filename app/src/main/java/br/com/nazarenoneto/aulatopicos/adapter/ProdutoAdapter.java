package br.com.nazarenoneto.aulatopicos.adapter;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.com.nazarenoneto.aulatopicos.R;
import br.com.nazarenoneto.aulatopicos.model.Produto;

public class ProdutoAdapter extends RecyclerView.Adapter<ProdutoAdapter.ProdutoViewHolder> {
    List<Produto> produtos;
    Resources resources;

    public ProdutoAdapter(List<Produto> produtos) {
        this.produtos = produtos;
    }

    @Override
    public ProdutoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_produto_layout, parent, false);
        ProdutoViewHolder itemVendaViewHolder = new ProdutoViewHolder(v);
        resources = v.getResources();
        return itemVendaViewHolder;
    }

    @Override
    public void onBindViewHolder(ProdutoViewHolder holder, int position) {
        Produto produto = produtos.get(position);
        holder.txtDescricao.setText(produto.getDescricao());
        holder.txtQuantidade.setText(String.valueOf(produto.getQuantidade()));
    }

    @Override
    public int getItemCount() {
        return produtos.size();
    }

    public static class ProdutoViewHolder extends RecyclerView.ViewHolder {
        TextView txtDescricao;
        TextView txtQuantidade;

        ProdutoViewHolder(View itemView) {
            super(itemView);
            txtDescricao = (TextView) itemView.findViewById(R.id.txtDescricao);
            txtQuantidade = (TextView) itemView.findViewById(R.id.txtQuantidade);
        }

    }
}
