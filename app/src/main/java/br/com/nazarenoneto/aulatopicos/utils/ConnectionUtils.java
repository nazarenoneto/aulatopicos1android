package br.com.nazarenoneto.aulatopicos.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;

public class ConnectionUtils {

    /* *
    * Função para verificar existência de conexão com a internet
    * Adicionar permissão abaixo
    * <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
    * */
    public static boolean verificaConexao(Activity activity) {
        boolean conectado;
        ConnectivityManager conectivtyManager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conectivtyManager.getActiveNetworkInfo() != null
                && conectivtyManager.getActiveNetworkInfo().isAvailable()
                && conectivtyManager.getActiveNetworkInfo().isConnected()) {
            conectado = true;
        } else {
            conectado = false;
        }
        return conectado;
    }

}
