package br.com.nazarenoneto.aulatopicos.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.rest.RestService;

import java.util.ArrayList;
import java.util.List;

import br.com.nazarenoneto.aulatopicos.R;
import br.com.nazarenoneto.aulatopicos.adapter.ProdutoAdapter;
import br.com.nazarenoneto.aulatopicos.controller.WebServiceClient;
import br.com.nazarenoneto.aulatopicos.model.Produto;
import br.com.nazarenoneto.aulatopicos.utils.RecyclerItemClickListener;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    private List<Produto> produtos;
    private ProdutoAdapter produtoAdapter;
    private RecyclerView recList;
    ProgressDialog progressDialog;
    @RestService
    WebServiceClient webServiceClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        getProdutos();
    }

    @AfterViews
    void init() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.app_title);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.dialog_produto);

                dialog.setTitle("Cadastro");

                final EditText edtDescricao = (EditText) dialog.findViewById(R.id.edtDescricao);
                final EditText edtQuantidade = (EditText) dialog.findViewById(R.id.edtQuantidade);

                Button btnConfirmar = (Button) dialog.findViewById(R.id.btnConfirmar);
                Button btnCancelar = (Button) dialog.findViewById(R.id.btnCancelar);

                btnConfirmar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edtDescricao.getText().toString().equals("")) {
                            Toast.makeText(getApplicationContext(), "Informe a descrição!", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if (edtQuantidade.getText().toString().equals("")) {
                            Toast.makeText(getApplicationContext(), "Informe a quantidade!", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Produto p = new Produto();
                        p.setDescricao(edtDescricao.getText().toString());
                        p.setQuantidade(Double.valueOf(edtQuantidade.getText().toString()));

                        salvarProduto(p);

                        dialog.dismiss();
                    }
                });

                btnCancelar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.show();

//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
            }
        });


        recList = (RecyclerView) findViewById(R.id.viewRota);
        recList.addOnItemTouchListener(new RecyclerItemClickListener(this, new OnItemClickListener()));
        recList.setHasFixedSize(true);

        produtos = new ArrayList<>();

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recList.setLayoutManager(llm);
        produtoAdapter = new ProdutoAdapter(produtos);
        recList.setAdapter(produtoAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            Toast.makeText(getApplicationContext(), "Refresh", Toast.LENGTH_SHORT).show();
            getProdutos();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Background
    void getProdutos() {
        mudaStatus(true, "Buscando informações");
        produtos = webServiceClient.getProdutos();
        for (Produto produto : produtos) {
            Log.v(MainActivity.class.getName(), produto.toString());
        }
        mudaStatus(false, "Buscando informações");
        atualizaTela();
    }

    @Background
    void salvarProduto(Produto produto) {
        mudaStatus(true, "Enviando informações");
        Log.v(MainActivity.class.getName(), "Salvar:" + produto.toString());
        webServiceClient.salvarProduto(produto);
        mudaStatus(false, "Enviando informações");
        getProdutos();
    }

    @Background
    void apagarProduto(Produto produto) {
        mudaStatus(true, "Enviando informações");
        Log.v(MainActivity.class.getName(), "Deletar:" + produto.toString());
        webServiceClient.deletarProduto(produto.getId());
        mudaStatus(false, "Enviando informações");
        getProdutos();
    }

    @UiThread
    void atualizaTela() {
        produtoAdapter = new ProdutoAdapter(produtos);
        recList.setAdapter(produtoAdapter);
    }

    @UiThread
    void mudaStatus(boolean mostra, String texto) {
        if (mostra) {
            progressDialog = ProgressDialog.show(this, "Atualizando",
                    texto, true);
        } else {
            progressDialog.dismiss();
        }
    }

    private class OnItemClickListener extends RecyclerItemClickListener.SimpleOnItemClickListener {

        @Override
        public void onItemClick(View childView, int position) {

        }

        @Override
        public void onItemLongPress(View childView, final int position) {

            final CharSequence[] items = {"Editar", "Apagar"};

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
//            builder.setTitle("Make your selection");
            builder.setItems(items, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int item) {
                    // Do something with the selection
                    if (items[item].equals("Editar")) {
                        final Dialog dialog = new Dialog(MainActivity.this);
                        dialog.setContentView(R.layout.dialog_produto);

                        dialog.setTitle("Editar");

                        final EditText edtDescricao = (EditText) dialog.findViewById(R.id.edtDescricao);
                        final EditText edtQuantidade = (EditText) dialog.findViewById(R.id.edtQuantidade);

                        final Produto produto = produtos.get(position);
                        edtDescricao.setText(produto.getDescricao());
                        edtQuantidade.setText(String.valueOf(produto.getQuantidade()));

                        Button btnConfirmar = (Button) dialog.findViewById(R.id.btnConfirmar);
                        Button btnCancelar = (Button) dialog.findViewById(R.id.btnCancelar);

                        btnConfirmar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (edtDescricao.getText().toString().equals("")) {
                                    Toast.makeText(getApplicationContext(), "Informe a descrição!", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                if (edtQuantidade.getText().toString().equals("")) {
                                    Toast.makeText(getApplicationContext(), "Informe a quantidade!", Toast.LENGTH_SHORT).show();
                                    return;
                                }


                                produto.setDescricao(edtDescricao.getText().toString());
                                produto.setQuantidade(Double.valueOf(edtQuantidade.getText().toString()));

                                salvarProduto(produto);

                                dialog.dismiss();
                            }
                        });

                        btnCancelar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        dialog.show();
                    }
                    if (items[item].equals("Apagar")) {
                        Produto produto = produtos.get(position);
                        apagarProduto(produto);
                    }
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

}
