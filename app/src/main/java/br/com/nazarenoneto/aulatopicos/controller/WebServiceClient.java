package br.com.nazarenoneto.aulatopicos.controller;

import org.androidannotations.annotations.rest.Delete;
import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Post;
import org.androidannotations.annotations.rest.Rest;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.List;

import br.com.nazarenoneto.aulatopicos.model.Produto;

@Rest(converters = {MappingJackson2HttpMessageConverter.class})
public interface WebServiceClient {
    @Get("http://10.0.2.2:8080/AulaTopicos/services/produto")
    public List<Produto> getProdutos();

    @Post("http://10.0.2.2:8080/AulaTopicos/services/produto")
    public void salvarProduto(Produto produto);

    @Delete("http://10.0.2.2:8080/AulaTopicos/services/produto/{id}")
    public void deletarProduto(int id);
}
